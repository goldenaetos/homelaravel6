<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    public function images(): HasMany
    {
        return $this->hasMany('App\ProductImage');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo('App\Category');
    }
}
